#!/usr/bin/env bash

# Gitlab CI runner cache
cachedir="$(pwd)/.nix-cache"
if [ -d "$cachedir" ]; then
	echo "extra-substituters = file://$cachedir?priority=10&trusted=true" \
		>>/etc/nix/nix.conf
fi

# Enable cachix if provided by environment
if [ -n "${CACHIX_NAME:-}" ]; then
	cachix use "$CACHIX_NAME"
fi

# Enable SSH substituter
if [ -n "${NIX_SSH_URL:-}" ] && [ -n "${NIX_SSH_KEY:-}" ]; then
	ssh-add - <<<"$NIX_SSH_KEY"
	echo "extra-substituters = ssh://$NIX_SSH_URL?priority=20&trusted=true" \
		>>/etc/nix/nix.conf
fi

# Enable S3 substituter
if [ -n "${NIX_S3_BUCKET:-}" ]; then
	echo "extra-substituters = s3://$NIX_S3_BUCKET?profile=${NIX_S3_PROFILE:-default}&priority=20&trusted=true" \
		>>/etc/nix/nix.conf
fi

# Save the current store content
find /nix/store -mindepth 1 -maxdepth 1 ! -name \*.drv |
	sort >/nix/.before
